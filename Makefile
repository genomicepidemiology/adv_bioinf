CFLAGS ?= -Wall -O3
CFLAGS += -std=c99

PROGS = zapGremlins

zapGremlins: zapGremlins.c
	$(CC) $(CFLAGS) -o $@ zapGremlins.c  $(LDFLAGS)

clean:
	$(RM) $(PROGS)
