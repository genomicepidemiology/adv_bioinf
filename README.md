# Introduction #



# Practical information #
Bioinformatics contains some elements of big data, which means that just copying things to your 
users might be a bad idea. To avoid filling the disk with unnecessary copies of data, please 
consider wether it is important to have your own copy of it.


# Welcome and introduction #
*Pimlapas Leekitcharoenphon (Shinny), Mon 20 Jan 9:00-9:30*



# Analysis pipeline - installation, fix the pipeline, run the pipeline and introduction to NextFlow #
*Rolf S. Kaas, Mon 20 Jan 9:30-16:30*



# Mapping and alignment using KMA #
*Philip T.L.C. Clausen, Tue 21 Jan 9:00-10:00*

*Optional reading: Philip T.L.C Clausen, Frank M. Aarestrup, Ole Lund "Rapid and precise alignment of raw reads against redundant databases with KMA", BMC Bioinformatics, 2018;19:307.*

Navigate to and list the content of the home directory and verify if "src/" and "bin/" exists, 
if not run following cmds:
```
mkdir src
mkdir bin
```

Depending on you operating system you will also have file called ".bashrc" or ".bash_profile" 
in your home directory. We will need to edit those so that you newly created directory "bin/" 
will be added to your path upon startup. This will enable you to execute your programs without 
the full path in the future.
Open the ".bashrc" or ".bash_profile" file using a flat text editor, such as "nano" or "pico".
Then check if "export PATH=$PATH:~/bin/" is in the file, if not add it and save it.
Any command added to this file will be executed on stratup when you open a new terminal window,
which means that you have to restart you terminal in order to effectualize your changes.

Now that you have directories to store your own source code and programs ("src/" and "bin/"), 
you are ready install your first own personal program for bioinformatic analysis completely 
independent of everybody else on the server / computer.
First you need to navigate to your source directory:
```
cd ~/src
```

Now we need to get some source code to install, in this case will install KMA which are a key 
component for several microbial bioinformatic programs. The source code for KMA is stored in 
Bitbucket on the address "https://bitbucket.org/genomicepidemiology/kma.git". On the Bitbucket
page there are some basic documentaion about KMA, which helps the user to understand and install 
the program. You can get the newest version of KMA with the command:
```
git clone https://bitbucket.org/genomicepidemiology/kma.git
```

Now go the repository and check content by skimming a few files:
```
cd kma
ls -l
less align.c
less penalties.h
```

As you can see you do not really have a program yet, actually what you got is a bunch of flat 
text files which makes up the source code for the program. The source code is the human readable 
version of the program, which we need to translate into computer readable code which then makes 
the program. You do this by compiling the code using the following command, given that the 
repository has as "Makefile" or "makefile".
```
make
```

As disussed earlier we need to move the program to our binary directory "bin/", so that we do 
not need the full path of the program in the future.
```
mv kma ~/bin/
```

Now you only need to verify the installation, basic test may include the following:
```
kma
kma -h
kma -v
```

You have now installed a piece of bioinformatic software which is one of the hardest tasks of 
being a bioinformatician, that calls for a 5 minute break.

Repeat the exercise on the cge-server.

OPTIONAL: In bioinformatics we often use a lot of time converting one format to another, which
from time to time is made by small custom scripts that are rewritten time over and time over. 
Some of the most annoying, frequent and general ones i see, i have made solutions to. These have 
been included in this repository, which you can install for yourself as well.
Start out by getting the repository, and install the source code in it.

As you see this comiles a program called "zapGremlins", this program removes hidden characters 
from a flat text file. This is usefull if you share code that has been written on different 
systems. The most frequent usecase for this is to remove Windows newlines from interpreted 
programs, such as Pyhon or Perl programs written on a Windows machine. Mv the program to your 
"bin/" directory to complete the installation. 

Finally there are three bash programs in the repository, as bash is an interpreted programing 
language we do not need to compile the source code. We just need to copy them to the "bin/" 
directory.
```
cp binit fq2int int2fq ~/bin/
```

The first program "binit" moves whatever argument given to the "bin/" directory, converting the 
"mv something ~/bin/" command to "binit something".
fq2int converts paired end fastq files to interleaved fastq format send to stdout, while int2fq 
does the opposite and saves the reult in two new files. An example of running those is:
```
fq2int sample_1.fq.gz sample_2.fq.gz | gzip -c > sample_int.fq.gz
int2fq sample_int.fq.gz
```

The programs "fq2int" and "int2fq" has an unbelievable number of free solutions in Python and 
Perl, but the caveat that they are about 50 times slower and some use unnecessary much memory. 

DONE


# CSIPhylogeny #
*Rolf S. Kaas, Tue 21 Jan 10:00-12:00*



# Genome quality checking pipeline (QC pipeline) - installation, fix the pipeline and run the pipeline #
*Rolf S. Kaas, Tue 21 Jan 12:00-17:00, Wed 22 Jan 13:00-16:30*



# Phylogenetic construction, visulizationa and interpretation #
*Pimlapas Leekitcharoenphon (Shinny), Wed 22 Jan 9:00-14:00*

Slide 1
https://silo1.sciencedata.dk/shared/579bb645a020541b8a31c2a368178692?download

Slide 2
https://silo1.sciencedata.dk/shared/83f835800d090d94a0909f42ba4dc22a?download


# Core and whole genome MLST (cgMLST and wgMLST) #
*Pimlapas Leekitcharoenphon (Shinny), Wed 22 Jan 14:00-17:00*

https://silo1.sciencedata.dk/shared/6be945f8a11e6bd1953f628eb45afa5c?download


# BEAST - Bayesian Evolutionary Analysis Sampling Tree #
*Pimlapas Leekitcharoenphon (Shinny), Thu 23 Jan 9:00-12:00*

https://silo1.sciencedata.dk/shared/5b1d58834ba248bfba1cffb70080fd4c?download

Shinny's DT104 paper
https://aem.asm.org/content/82/8/2516.short

A hands-on practical on Phylogeographic inference in discrete space
https://www.biostat.washington.edu/sites/default/files/modules//2016_SISMID_13_16.pdf



# Mobile Genetic Elements #
*Markus H.K. Johansson, Thu 23 Jan 13:00-15:00*



# Pan/core genome analysis #
*Pimlapas Leekitcharoenphon (Shinny), Thu 23 Jan 15:00-16:30*

https://silo1.sciencedata.dk/shared/813bd5f89a35b359d4414397542d4c67?download


# Long read bioinformatics #
*Philip T.L.C. Clausen, Fri 24 Jan 9:00-14:00*

*Optional reading: Philip T.L.C Clausen, Frank M. Aarestrup, Ole Lund "Rapid and precise alignment of raw reads against redundant databases with KMA", BMC Bioinformatics, 2018;19:307.*  
*Zankari E, Hasman H, Cosentino S, Vestergaard M, Rasmussen S, Lund O, Aarestrup FM, Larsen MV. "Identification of acquired antimicrobial resistance genes", J Antimicrob Chemother, 2012 Jul 10.*  
*Hasman H, Saputra D, Sicheritz-Pontén T, Lund O, Svendsen CA, Frimodt-Møller N, Aarestrup FM "Rapid whole-genome sequencing for detection and characterization of microorganisms directly from clinical samples", J Clin Microbiol, 2014 Jan;52(1):139-46.*  
*Heng Li "Minimap and miniasm: fast mapping and de novo assembly for noisy long sequences", Bioinformatics, Volume 32, Issue 14, 15 July 2016*  
*Heng Li "Minimap2: pairwise alignment for nucleotide sequences", Bioinformatics, Volume 34, Issue 18, 15 September 2018*  

The exercise here assumes that the "Mapping and alignment using KMA" exercise has been succesfully 
completed, so that KMA now exists in your path.
If anything is written in all CAPITALS on grey bckground, it means that you have to change something.

SOFTWARE:
In addition to KMA we need to install some extra programs to analyze the MinION data presented
today. Which includes: miniasm, minimap2, ccphylo and polisher.pl.
Start out by installing miniasm, minimap2 and ccphylo from their source code, their 
repositories are:
```
https://github.com/lh3/miniasm.git
https://github.com/lh3/minimap2.git
https://bitbucket.org/genomicepidemiology/ccphylo.git
```

Finally there is a wrapper script called "polisher.pl" in the course repository 
which we would like in our path as well. "polisher.pl" will help making the Nanopore assemblies.


DATABASES:
Lets make a "database/" directory next our "src/" and "bin/", in which we can store small 
specialized databases.
```
cd ~
mkdir database
```

The small specialized databases hosted by CGE is also stored at bitucket. Now get the ResFinder 
database included in the "database/" directory.
```
cd databases
git clone https://git@bitbucket.org/genomicepidemiology/resfinder_db.git
```

Before we can use the ResFinder database with KMA, we need to convert the fasta files to a KMA index.
```
cd resfinder_db
kma index -i *.fsa -o ResFinder
```

Other databases are too large for each one of us to have our own copy of it. An example of this could 
be the KmerFinder bacteria database, which contains the complete genomes of all the bacteria on NCBI.
The larger databases, such the KmerFinder ones, hosted by CGE is located at:
```
http://www.cbs.dtu.dk/public/CGE/databases/
```

The bacteria database that we are going to use today is here:
```
http://www.cbs.dtu.dk/public/CGE/databases/KmerFinder/version/20190108_stable/bacteria.tar.gz
```

If you want, you can download and analyze it on your own computer if it has 6 GB or more RAM. 
Alternatively we have a copy of it on cgebase08, for this exercise.

DATA:
For todays exercise we have a few isolates that has been sequenced both on Illumina and MinION, so that 
there is something to compare with. The data for todays exercise is collected at:
```
# Illumina data
/home/adv_bioinf/data/nanoporeExercise/illumina_data/

# MinION data
/home/adv_bioinf/data/nanoporeExercise/minion_data/

# Reference genome
/home/adv_bioinf/data/nanoporeExercise/reference/

# KmerFinder DB
/home/adv_bioinf/database/KmerFinder/bacteria.ATG
```

For the assembly, species identification and alignment exercise it is recommended to choose 1 isolate
from the Illumina and Nanopore data directories. Remember to choose the same isolate for two technologies 
so that you have something to compare with, and DO NOT copy them to your own home (it is big files).

ASSEMBLY:
Currently we have two options for making our Nanopore assemblies, CANU which highly precise but takes 
a lot of computational resources, or quick and dirty draft assembly followed by polishing.
Today we are going to make the latter, as that can answer the most basic questions. Generally, the 
assemblies opens a big box of tools to use later, one should however remember that these Nanopore 
assemblies are of a different quality then the Illumina ones. Which ultimately means you have to accept
some extra indels when compared to Illumina, on the plus side the Nanopore assemblies are often more 
complete and do not contain large gaps as is known from Illumina.
Lets get to it. The program "polisher.pl" uses Minimap2 to identify overlaps between the individual 
Nanopore reads, whereafter these are pieced together by Miniasm. This gives a rough assembly of the 
reads, which we then iteratively polish with KMA to get rid of some the errors. This means that 
"polisher.pl" is essentially pushing a lot of buttons for us, making the whole process look more simple.
"polisher.pl" is run as:
```
polisher.pl -nano /home/adv_bioinf/data/nanoporeExercise/minion_data/YOURSAMPLE -o SOME/OUTPUT/DIRECTORY/YOURSAMPLE -clean
```

SPECIES IDENTIFICATION:
The first and probably most basic question you will be asked about your sequence data is: "What is it".
As it has already been sequenced you basically whether it is bacteria, virus, archea, fungi etc. But 
we really like to know taxonomy down to at species level, at least for WGS samples. To answer this 
question we can use KmerFinder, or actually go one step benief it and KMA which runs in the background 
of KmerFinder.

Run KMA with the KmerFinder database your raw read data and assembly data, and compare the results.
```
kma -i /home/adv_bioinf/data/nanoporeExercise/minion_data/YOURSAMPLE -o SOME/OUTPUT/DIRECTORY/YOURSAMPLE -t_db /home/adv_bioinf/database/KmerFinder/bacteria.ATG -Sparse

kma -ipe /home/adv_bioinf/data/nanoporeExercise/illumina_data/YOURSAMPLES -o SOME/OUTPUT/DIRECTORY/YOURSAMPLE -t_db /home/adv_bioinf/database/KmerFinder/bacteria.ATG -Sparse

kma -i YOUR/NANOPORE/ASSEMBLY -o SOME/OUTPUT/DIRECTORY/YOURSAMPLE -t_db /home/adv_bioinf/database/KmerFinder/bacteria.ATG -Sparse
```

Does the results match, if so what is the species, if not what seems to be difference. 

ALIGNMENT:
Compared to the species identification, we need to tweak a bit more parameters when aligning Nanopore 
reads compared to when we analyze Illumina reads. 
Below is the setup of how to align against the ResFinder database using KMA on the different technologies,
try look up each of the option and reason why thay are needed for each technology with ResFinder as an 
example of database. 
Would the selection of options be any different if we were aligning against whole genomes.

Try to align against the ResFinder database:
```
kma -mem_mode -bcNano -bc 0.7 -i /home/adv_bioinf/data/nanoporeExercise/minion_data/YOURSAMPLE -o SOME/OUTPUT/DIRECTORY/YOURSAMPLE -t_db ~/database/resfinder_db/ResFinder

kma -cge -1t1 -mem_mode -apm p -ipe /home/adv_bioinf/data/nanoporeExercise/illumina_data/YOURSAMPLES -o SOME/OUTPUT/DIRECTORY/YOURSAMPLE -t_db ~/database/resfinder_db/ResFinder

kma -i YOUR/NANOPORE/ASSEMBLY -o SOME/OUTPUT/DIRECTORY/YOURSAMPLE -t_db ~/database/resfinder_db/ResFinder
```

Is there any major differences in the results, or the results comparable. What seems to be the biggest 
issue when comparing the results, from the different technologies.


SNP ANALYSIS:
Finally it is time to check whether your sample has been seen before, or if it is something new. 
Traditionally this is performed using a SNP analysis, and that is exactly what we are going to perform 
on our MinION data. As mentioned earlier, the error-rate of MinION and error pattern is slightly 
different from the 2nd generation sequence data, which means that we have to tweak our analysis compared 
to what we have been used to with e.g. Illumina.
As always we will start our SNP analysis out with some alignments, here for the MinION samples.

```
kma -Mt1 1 -bcNano -bc 0.7 -1t1 -dense -ref_fsa -ca -i EACH/MINION/SAMPLE/ON/BY/ONE -o SOME/OUTPUT/DIRECTORY/MINION/SAMPLE -t_db /home/adv_bioinf/data/nanoporeExercise/reference/uhauhaReference
```
Can you reason with the non-default settings on the alignment.

When all the alignments have been performed we can compare the consensus sequences between the samples, 
this task is made easy with CCPhylo.

```
ccphylo dist -r "uha uha Reference" -i SOME/OUTPUT/DIRECTORY/MINION/SAMPLE*.fsa -o SOME/OUTPUT/DIRECTORY/MINION.phy -nm 0 -pr 10
```

"ccphyo dist" creates a distance matrix telling us how many SNPs there are between each sample. This 
distance can be visualized in a phylogenetic tree if we transform using Neighbour-Joining (NJ). NJ 
has also been implemented in CCPhylo, and is like:

```
ccphylo tree -i SOME/OUTPUT/DIRECTORY/MINION.phy -o SOME/OUTPUT/DIRECTORY/MINION.nwck
```

The output from this implementation of NJ is in newick format, which in essence is the representation 
of the phylogenetic tree. For a more humane visualization programs like FigTree can be used, available 
here: 
```
http://tree.bio.ed.ac.uk/software/figtree/
```

Now that your SNP analysis is done, does it seem that there is an outbreak going on and if so are your 
sample part of it. Typically we use threshold of 10 SNPs to define an outbreak for *E. coli*, but there 
is no hard threshold that has been agreed upon internationally.
Now that this tree is based on MinION data there will probably be some hypocrats that do not believe 
your analysis results, as he or she only belive in Illumina based SNP analysis. Unfortunately we will 
always have to deal with ignorant fools, so to please this time we make the equivalent analysis with 
Illumina data.

Start out by aligning the sequences:
```
kma  -Mt1 1 -1t1 -dense -ref_fsa -ca -ipe EACH/ILLUMINA/SAMPLE/ON/BY/ONE -o SOME/OUTPUT/DIRECTORY/MINION/SAMPLE -t_db /home/adv_bioinf/data/nanoporeExercise/reference/uhauhaReference
```
As before see if you can reason with the settings used on the alignment.
The remaining part of the analysis with CCPhylo is the same on Illumina as for MinION. Complete these 
analyses and compare your results from the two different sequence technologies.

Optional: For this SNP analysis you were given the rererence genome to make the basis for your SNP
analysis. If you have not been given this, how would you then have proceeded with the analysis.

DONE


# Case study and application of bioinformatics tools #
*Pimlapas Leekitcharoenphon (Shinny), Fri 24 Jan 14:00-16:00*

https://silo1.sciencedata.dk/shared/426af1f7cd41160d9cf02443f62b94bf?download

# Evaluation form #
*Please fill out an evaluation form from the following link*

https://www.surveymonkey.com/r/AdvBioInfo2020

