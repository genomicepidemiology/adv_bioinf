#!/usr/bin/env python
"""plot MGEs."""

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
from tabulate import tabulate
import numpy as np


MGE_TR = {'mite': 'miniature inverted repeats',
          'is': 'insertion sequence',
          'tn': 'unit transposon',
          'cn': 'composite transposon',
          'tn': 'unit transposon',
          'ice': 'intergrative conjugative elements',
          'ime': 'intergrative mobilizable elements'}


def plot_clustered_stacked(dfall, labels=None, ax=None, title="multiple stacked bar plot",  H="/", **kwargs):
    """Given a list of dataframes, with identical columns and index, create a clustered stacked bar plot.

    labels is a list of the names of the dataframe, used for the legend
    title is a string for the title of the plot
    H is the hatch used for identification of the different dataframe"""

    n_df = len(dfall)
    n_col = len(dfall[0].columns)
    n_ind = len(dfall[0].index)

    if ax is None:
        ax = plt.subplot(111)

    for df in dfall : # for each data frame
        ax = df.plot(kind="bar",
                     linewidth=0.5,
                     stacked=True,
                     ax=ax,
                     legend=False,
                     grid=False,
                     **kwargs)  # make bar plots

    h, l = ax.get_legend_handles_labels() # get the handles we want to modify
    for i in range(0, n_df * n_col, n_col): # len(h) = n_col * n_df
        for j, pa in enumerate(h[i:i+n_col]):
            for rect in pa.patches: # for each index
                rect.set_x(rect.get_x() + 1 / float(n_df + 1) * i / float(n_col))
                rect.set_hatch(H * int(i / n_col)) #edited part
                rect.set_width(1 / float(n_df + 1))

    ax.set_xticks((np.arange(0, 2 * n_ind, 2) + 1 / float(n_df + 1)) / 2.)
    ax.set_xticklabels(df.index, rotation = 0)
    ax.set_title(title)

    # Add invisible data to add another legend
    n=[]
    for i in range(n_df):
        n.append(ax.bar(0, 0, color="gray", hatch=H * i))

    l1 = ax.legend(h[:n_col], l[:n_col], loc=[1.01, 0.5])
    if labels is not None:
        l2 = plt.legend(n, labels, loc=[1.05, 0.8])
    ax.add_artist(l1)
    return ax


def plot_binned_per_mge_type(combined_mgefinder, output):
    """Plot number of MGEs per type per primary source."""
    map_name = 'tab10'
    cmap = matplotlib.cm.get_cmap(map_name)
    mge_types = combined_mgefinder['mge_type'].unique()
    norm = matplotlib.colors.Normalize(vmin=0, vmax=len(mge_types))
    type_col = {c: cmap(norm(i)) for i, c in enumerate(mge_types)}

    # Number of MGEs barplot
    mge_cnt = (combined_mgefinder
               .query('not putative_cn')
               .groupby(['primary_source', 'country', 'mge_type'])
               .apply(len)
               .unstack()
               .fillna(0))

    # subset on primary source
    pigs = mge_cnt.loc['pigs'].T
    pigs = pigs.reindex(sorted(pigs.columns), axis=1)
    broilers = mge_cnt.loc['broilers'].T
    broilers['Germany'] = 0
    broilers = broilers.reindex(pigs.columns, axis=1)

    # define plot
    fig = plt.figure(figsize=(7, 6), dpi=300)
    gs = fig.add_gridspec(2, 2)
    axes = [fig.add_subplot(gs[0, :]),
            fig.add_subplot(gs[-1, 0]),
            fig.add_subplot(gs[-1, 1]),
    ]

    # plot number of predicted mges
    plot_clustered_stacked([pigs, broilers], labels=['pigs', 'broilers'],
                           ax=axes[0], H='//', edgecolor='black',
                           linestyle='-', title='')

    axes[0].set_yscale('log')
    axes[0].set_ylabel('#MGEs (log10)')
    axes[0].set_xlabel('')
    axes[0].set_title('#MGEs ~ type, country and primary source')

    # plot number of mges per sample
    for i, prim_source in enumerate(['pigs', 'broilers'], start=1):
        s = f'not putative_cn and primary_source == "{prim_source}"'
        mges_gr = (combined_mgefinder
                   .query(s)
                   .groupby(['mge_type', 'sample_name'])
                   .apply(len).reset_index(0))

        # Boxplot
        a = sns.boxplot(x='mge_type', y=0, data=mges_gr, orient='v',
                        palette=map_name, ax=axes[i])
        if prim_source == 'broilers':
            for a in a.artists:
                a.set(hatch='//')

        axes[i].set_title(prim_source)

        # change face color
        atribs = [x.get_text() for x in axes[i].get_xticklabels()]
        for attr, patch in zip(atribs, axes[i].findobj(matplotlib.patches.Patch)):
            patch.set_facecolor(type_col[attr])

        # translate xtic labels
        #new_labels = [MGE_TR[x.get_text()] for x in axes[i].get_xticklabels()]
        #axes[i].set_xticklabels(new_labels, rotation = 90)

        # Modify plot
        axes[i].set_title(f'{prim_source.capitalize()}')
        axes[i].set_xlabel('')

    # set shared ylim for boxplots
    axes[1].set_ylim(0, 13)
    axes[2].set_ylim(0, 13)
    axes[1].set_ylabel('#MGEs per sample')
    axes[2].set_ylabel('')

    # write plot
    plt.tight_layout()
    plt.savefig(output)
    print(f'wrote plot: {output}')
    plt.close()


def plot_num_mges_per_st(combined_mgefinder, output_path):
    """Plot number of MGEs per sequence type."""
    num_mges = (combined_mgefinder
                .set_index('sample_name')
                .query('not putative_cn')
                .groupby('sample_name')
                .apply(len))

    cols = ['primary_source', 'country', 'st']
    num_mges = (pd.DataFrame(num_mges)
                .join(combined_mgefinder.set_index('sample_name')[cols])
                .reset_index()
                .rename({'index': 'sample_name', 0: '#MGEs'}, axis=1))
    num_mges = num_mges.drop_duplicates()

    num_mges.columns = [c.replace('_', ' ') for c in num_mges.columns]
    num_mges['st'] = num_mges['st'].apply(lambda x: f'st{x}')

    thres = 12
    common_st = num_mges['st'].value_counts().head(thres).index
    num_mges_common = num_mges[num_mges['st'].isin(common_st)]

    fig, axes = plt.subplots(2, 1, figsize=(4, 6), dpi=300, sharex=True)
    # Boxplot
    sns.boxplot(x='st', y='#MGEs', data=num_mges_common,
                order=list(common_st),
                ax=axes[0])
    axes[0].set_title('#MGEs per sample per MLST st')

    # Stacked barplot with common sequence types
    (num_mges[num_mges['st'].isin(common_st)]
     .groupby(['st', 'country'])
     .apply(len)
     .unstack()
     .loc[common_st]
     .plot(kind='bar', stacked=True, ax=axes[1]))

    axes[1].set_ylabel('#Samples')

    fig.tight_layout()
    plt.savefig(output_path)
    print(f'wrote plot: {output_path}')
    plt.close(fig)


def plot_transposons_per_st(combined_mgefinder, output_path):
    """Plot the number of transposons per sequence type."""
    cols = ['sample_name', 'contig_name', 'mge_name', 'mge_type', 'st']
    filtered_mges = combined_mgefinder.query('not putative_cn')
    filtered_mges = filtered_mges[filtered_mges['mge_type'].isin(['tn', 'cn', 'ice', 'ime'])][cols]

    fig, axes = plt.subplots(1, 1, dpi=300)
    filtered_mges.groupby(['mge_name', 'st']).apply(len).unstack().plot.bar(stacked=True, ax=axes)
    axes.set_xlabel('Transposon name')
    axes.set_ylabel('#MGEs')
    axes.set_title('#Transposons ~ MLST st')

    fig.tight_layout()
    print(f'wrote plot: {output_path}')
    plt.savefig(output_path)
    plt.close(fig)


def plot_putative_composite_tn(combined_mgefinder, output_path):
    """Plot composite transposons."""
    num_samples = len(combined_mgefinder['sample_name'].unique())

    fig, axes = plt.subplots(1, 2, figsize=(6, 4), dpi=300)

    # plot most comomn infered cn
    num_mges = 5
    filtered_mges = combined_mgefinder.query('putative_cn')
    filtered_mges['mge_name'].value_counts().head(num_mges).plot(kind='bar', ax=axes[0])
    axes[0].set_title('Number composite tn')
    axes[0].set_ylabel('#Composite tn')
    axes[0].set_xlabel('Name')

    # plot length distribution of composite tn
    cn_info = pd.DataFrame(filtered_mges['mge_name'].apply(lambda x: x.split('_')[2])).rename({'mge_name': 'flanking mge'}, axis=1)
    cn_info['length'] = filtered_mges['mge_name'].apply(lambda x: x.split('_')[1]).astype(int)
    #sns.boxplot(y='length', x='flanking mge', data=cn_info, ax=axes[1])
    sns.swarmplot(y='length', x='flanking mge', data=cn_info, ax=axes[1])
    axes[1].set_title('MGE length')
    axes[1].set_ylabel('Length (log10)')
    axes[1].set_xlabel('flanking insertion sequence')

    # configure plot
    fig.suptitle('')
    fig.tight_layout()

    plt.savefig(output_path)
    print(f'wrote plot: {output_path}')
    plt.close(fig)


if __name__ == '__main__':
    # read data
    combined_mgefinder = pd.read_csv('mgefinder_combined_results.csv')
    # plot number of mges per type and primary source
    plot_binned_per_mge_type(combined_mgefinder,
                             'num_mges_per_type_primary_source.png')

    # plot number of mges per mlst st
    output_path = 'num_mges_per_st.png'
    plot_num_mges_per_st(combined_mgefinder, output_path)

    # plot number of mobile elements
    output_path = 'num_transposons_carried_per_st.png'
    plot_transposons_per_st(combined_mgefinder, output_path)

    # plot number of infered composite transposons
    output_path = 'num_putative_composite_tn.png'
    plot_putative_composite_tn(combined_mgefinder, output_path)

    num_samples = len(combined_mgefinder['sample_name'].unique())

    # calculate average copy number
    mean_copy_number = (combined_mgefinder
                        .groupby(['mge_name', 'sample_name'])
                        .apply(len)
                        .reset_index(1)
                        .groupby('mge_name')
                        .mean()
                        .round(1)
                        .rename({0: 'count'}, axis=1))

    for mge_type in combined_mgefinder['mge_type'].unique():
        mge_profile = (combined_mgefinder
                       .query(f'mge_type == "{mge_type}"')
                       .groupby(['sample_name', 'mge_name'])
                       .apply(len)
                       .unstack()
                       .fillna(0)
                       .applymap(lambda x: 1 if x > 0 else 0))
        sub = (pd.DataFrame(mge_profile.sum())
               .join(combined_mgefinder.set_index('mge_name'))
               .reset_index()
               .rename({'mge_name': 'Name', 0: '#MGEs', 'mge_family': 'Family', 'index': 'Name'}, axis=1))
        # add prevalence
        sub['Prevalence [%]'] = (sub['#MGEs'] / num_samples * 100).round(1)
        sub['Mean Copy number'] = sub['Name'].apply(lambda x: mean_copy_number.loc[x, 'count'])
        sub = (sub[['Name', 'Family', 'Prevalence [%]', 'Mean Copy number']]
               .drop_duplicates()
               .sort_values('Prevalence [%]', ascending=False))

        print()
        print(MGE_TR[mge_type])
        print(tabulate(sub, headers='keys', tablefmt='psql', showindex=False))
