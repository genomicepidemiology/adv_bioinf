#!/usr/bin/env python
"""Process data."""
import pandas as pd
import json
import sys
import pathlib

MGE_TR = {'mite': 'miniature inverted repeats',
          'is': 'insertion sequence',
          'tn': 'unit transposon',
          'cn': 'composite transposon',
          'tn': 'unit transposon',
          'ice': 'intergrative conjugative elements',
          'ime': 'intergrative mobilizable elements'}


def get_data_dir(*path):
    """Get data directory."""
    DATA_DIR = pathlib.Path(__file__).absolute().parent.parent.joinpath('data')
    return str(DATA_DIR.joinpath(*path))


def build_dataset(data):
    """Convert json information to dataframe."""
    res = []
    for mge_res in data:
        for result in mge_res['result']:
            s_start = int(
                min(result['alignment'],
                    key=lambda x: x['subject_start'])['subject_start'])
            s_end = int(
                max(result['alignment'],
                    key=lambda x: x['subject_end'])['subject_end'])

            res.append({
                'sample_name':
                mge_res['meta']['sample'],
                'mge_name':
                result['name'],
                'mge_id':
                result['mge_id'],
                'mge_family':
                result['family'],
                'mge_type':
                result['type'],
                'contig_name':
                result['contig'],
                'putative_cn':
                result['evidence'] == 2,
                'strand':
                result['strand'],
                'num_hsps':
                result['num_hsps'],
                'coverage':
                result['coverage'],
                'identity':
                result['identity'],
                'e_value':
                result['e_value'],
                'gaps':
                result['gaps'],
                '5p_trunc_len':
                s_start - 1 if result['template_length'] else None,
                '3p_trunc_len':
                result['template_length'] -
                s_end if result['template_length'] else None,
                'substitutions':
                result['substitutions'],
                'depth':
                result['depth'],
                'start':
                result['start'],
                'end':
                result['end'],
                'allele_seq_length':
                result['allele_seq_length'],
                'template_length':
                result['template_length'],
            })
    return pd.DataFrame(res)


def _read_sample(path):
    with open(path) as i:
        return json.load(i)


assert len(sys.argv) > 2, 'to few input files was specified'
_, samples_info, *mgefinder_result = sys.argv


samples_info = pd.read_csv(samples_info)

data = [_read_sample(result_path)
        for result_path in mgefinder_result]
mgefinder = build_dataset(data)

# add information on primary source, sequence type and
cols = ['country', 'primary_source', 'st']
mge_result = (mgefinder.set_index('sample_name').join(samples_info.set_index('sample_name')[cols]))

# write output
with open('mgefinder_combined_results.csv', 'w') as o:
    o.write(mge_result.to_csv())
