/* Philip T.L.C. Clausen Jan 2017 plan@dtu.dk */

/*
 * Copyright (c) 2017, Philip Clausen, Technical University of Denmark
 * All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *              http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]) {
	
	int c;
	char *filename;
	FILE *in;
	
	if(argc != 2) {
		fprintf(stderr, "Need a filename, and nothing but the filename.\n");
		return 1;
	} else if(*(filename = *++argv) == '-' && filename[1] == 'h' && filename[2] == 0) {
		fprintf(stderr, "# Need a filename, and nothing but the filename.\n");
		return 0;
	}
	if(!(in = fopen(filename, "rb"))) {
		fprintf(stderr, "Filename:\t%s\n", filename);
		fprintf(stderr, "Error: %d (%s)\n", errno, strerror(errno));
		return errno;
	}
	while((c = fgetc(in)) != EOF) {
		if((isprint(c) || isspace(c)) && c != '\r') {
			fprintf(stdout, "%c", c);
		}
	}
	fclose(in);
	
	return 0;
}
