#!/usr/bin/perl -w
#
# Philip T.L.C. Clausen Jan 2018 plan@dtu.dk
#
# Copyright (c) 2017, Philip Clausen, Technical University of Denmark
# All rights reserved.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#              http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

use strict;

sub sysCheck {
	
	my ($cmd, $error) = @_;
	my $errno;
	print STDOUT sprintf("# %s\n", $cmd);
	$errno = system($cmd);
	if($errno) {
		print STDERR sprintf("%s\n", $error);
		exit($errno);
	}
}

sub helpMessage {
	
	my ($status) = @_;
	my ($OUT);
	
	if($status) {
		$OUT = *STDERR;
	} else {
		$OUT = *STDOUT;
	}
	
	print $OUT "polisher uses Minimap, Miniasm and KMA to produce asemblies of nanopore reads.\n";
	print $OUT "Options:\n";
	print $OUT "\t-nano\t\tNanopore reads.\n";
	print $OUT "\t-illumina\tIllumina reads.\n";
	print $OUT "\t-o\t\tOutput. [-nano]\n";
	print $OUT "\t-t\t\tNumber of threads [1]\n";
	print $OUT "\t-mi\t\tMax number of iterations [5]\n";
	print $OUT "\t-minimap_opt\tMinimap2 options [\"-X -x ava-ont\"]\n";
	print $OUT "\t-kma_opt\tKMA options [\"-mem_mode -1t1 -bcg -bc 0.7 -ca -apm f\"]\n";
	print $OUT "\t-kma_opt_init\tKMA initial options [\"-mem_mode -1t1 -bcg -ca\"]\n";
	print $OUT "\t-clean\t\tRm intermediate files.\n";
	print $OUT "\t-h\t\tShows this help message.\n";
	
	exit($status);
}

sub main {
	my ($argc, @argv) = @_;
	
	# define variables
	my ($args, $status, $t, $i, $maxIter, $clean); # int
	my ($arg, $cmd, $outputfilename, $nano_read, @illumina_read); # char
	my ($minimap_opt, $kma_opt, $kma_opt_init); # char
	
	# -mem_mode -1t1 -bcg -bc 0.7 -ca -apm f -mq 25 -bcd 10
	# -mem_mode -ca -1t1 -mrs 0.0 -apm f 
	# -mem_mode -ca -1t1 -mrs 0.0 -bcg -mq 10 -bcd 20 -apm f 
	
	# -mem_mode -ca -1t1 -bcg -mq 10 -bcd 20 -apm f 
	# -mem_mode -ca -1t1 -bcg -bc 0.5 -mq 10 -bcd 10 -apm f
	
	# set defaults
	$maxIter = 5;
	$t = 1;
	$clean = 0;
	$nano_read = "";
	$outputfilename = "";
	$minimap_opt = "-X -x ava-ont"; # "-Sw5 -L100 -m0";
	$kma_opt = "-mem_mode -ca -1t1 -bcg -mq 1 -bcd 10 -apm f";
	$kma_opt_init = "-mem_mode -ca -1t1 -bcg";
	@illumina_read = ();
	
	# parse cmd-line
	$args = -1;
	while(++$args < $argc) {
		$arg = $argv[$args];
		if($arg eq "-nano") {
			if(++$args < $argc) {
				$nano_read = $argv[$args];
			}
		} elsif($arg eq "-o") {
			if(++$args < $argc) {
				$outputfilename = $argv[$args];
			}
		} elsif($arg eq "-illumina") {
			while(++$args < $argc && rindex($argv[$args], '-', 0) != 0) {
				push(@illumina_read, $argv[$args]);
			}
			if($args != $argc) {
				--$args;
			}
		} elsif($arg eq "-t") {
			if(++$args < $argc) {
				$t = $argv[$args] + 0;
			}
		} elsif($arg eq "-mi") {
			if(++$args < $argc) {
				$maxIter = $argv[$args] + 0;
			}
		} elsif($arg eq "-minimap_opt") {
			if(++$args < $argc) {
				$minimap_opt = $argv[$args];
			}
		} elsif($arg eq "-kma_opt") {
			if(++$args < $argc) {
				$kma_opt = $argv[$args];
			}
		} elsif($arg eq "-kma_opt_init") {
			if(++$args < $argc) {
				$kma_opt_init = $argv[$args];
			}
		} elsif($arg eq "-clean") {
			$clean = 1;
		} elsif($arg eq "-h") {
			&helpMessage(0);
		} else {
			print STDERR sprintf("Invalid option:\t%s\nUse \"-h\" for help.\n", $arg);
			exit(1);
		}
	}
	
	if($argc == 0) {
		&helpMessage(1);
	} elsif(length($nano_read) == 0) {
		print STDERR "No nanopore reads specified.\n";
		exit(1);
	}
	if(length($outputfilename) == 0) {
		$outputfilename = $nano_read;
	}
	
	# Make draft assembly with miniasm
	print STDERR "# Running initial minimap2.\n";
	$cmd = sprintf("minimap2 -t %d %s %s %s > %s.paf", $t, $minimap_opt, $nano_read, $nano_read, $outputfilename);
	&sysCheck($cmd, "minimap2 failed.");
	print STDERR "# Running miniasm.\n";
	$cmd = sprintf("miniasm -f %s %s.paf > %s.gfa", $nano_read, $outputfilename, $outputfilename);
	&sysCheck($cmd, "miniasm failed.");
	print STDERR "# Generating initial fasta.\n";
	$cmd = sprintf("grep LN:i: %s.gfa | awk \'{print \">\"\$2\"_\"\$4; print \$3}\' > %s.gfa.fsa && rm -f %s.gfa", $outputfilename, $outputfilename, $outputfilename);
	&sysCheck($cmd, "Generating initial fasta failed.");
	
	# Polish with kma
	print STDERR "# Running initial KMA polish.\n";
	$cmd = sprintf("kma index -i %s.gfa.fsa -o %s.0", $outputfilename, $outputfilename);
	&sysCheck($cmd, "Initial KMA index failed.");
	$cmd = sprintf("kma -t %d %s -i %s -o %s.0 -t_db %s.0", $t, $kma_opt_init, $nano_read, $outputfilename, $outputfilename);
	&sysCheck($cmd, "Initial KMA polish failed.");
	print STDERR "# Running iterative KMA polish.\n";
	$i = 0;
	while($i < $maxIter) {
		$cmd = sprintf("kma index -i %s.%d.fsa -o %s.%d", $outputfilename, $i, $outputfilename, $i + 1);
		++$i;
		&sysCheck($cmd, "Iterative KMA index failed.");
		$cmd = sprintf("kma -t %d %s -i %s -o %s.%d -t_db %s.%d", $t, $kma_opt, $nano_read, $outputfilename, $i, $outputfilename, $i);
		&sysCheck($cmd, "Iterative KMA polish failed.");
	}
	
	# Polish with illumina reads
	if(scalar(@illumina_read) != 0) {
		print STDERR "# Running illumina KMA polish.\n";
		$cmd = sprintf("kma index -i %s.%d.fsa -o %s.illumina", $outputfilename, $maxIter, $outputfilename);
		&sysCheck($cmd, "Iterative KMA index failed.");
		if(scalar(@illumina_read) == 1) {
			$cmd = sprintf("kma -t %d %s -bcd 1 -i %s.%d.fsa %s -o %s.illumina -t_db %s.illumina", $t, $kma_opt, $outputfilename, $maxIter, $illumina_read[0], $outputfilename, $outputfilename);
		} elsif(scalar(@illumina_read) == 2) {
			$cmd = sprintf("kma -t %d %s  -bcd 1 -i %s.%d.fsa -ipe %s -o %s.illumina -t_db %s.illumina", $t, $kma_opt, , $outputfilename, $maxIter, join(' ', @illumina_read), $outputfilename, $outputfilename);
		} elsif(scalar(@illumina_read) == 3) {
			$cmd = sprintf("kma -t %d %s  -bcd 1 -ipe %s -i %s %s.%d.fsa -o %s.illumina -t_db %s.illumina", $t, $kma_opt, $illumina_read[0], $illumina_read[1], $illumina_read[2], $outputfilename, $maxIter, $outputfilename, $outputfilename);
		} else {
			print STDERR "Invalid number of illumina reads.\n";
			exit(1);
		}
		&sysCheck($cmd, "Illumina KMA polish failed.");
		$cmd = sprintf("%s.illumina.fsa", $outputfilename);
	} else {
		$cmd = sprintf("%s.%d.fsa", $outputfilename, $maxIter);
	}
	rename($cmd, sprintf("%s.fsa", $outputfilename));
	
	print STDERR "# Assembly is ready.\n";
	
	# clean up
	if($clean) {
		# clean miniasm
		$cmd = sprintf("rm -f %s.paf %s.gfa.fsa", $outputfilename, $outputfilename);
		system($cmd);
		
		# clean iteratives
		$i = 0;
		while($i <= $maxIter) {
			$cmd = sprintf("rm -f %s.%d.*", $outputfilename, $i);
			system($cmd);
			++$i;
		}
		
		# clean illumina
		if(scalar(@illumina_read) != 0) {
			$cmd = sprintf("rm -f %s.illumina.*", $outputfilename);
			system($cmd);
		}
	}
	
	return 0;
}

&main(scalar(@ARGV), @ARGV);
